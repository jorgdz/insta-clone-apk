package com.jorgdz.instaclone.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.transition.Explode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.jorgdz.instaclone.R;
import com.jorgdz.instaclone.pojo.Picture;
import com.jorgdz.instaclone.view.PictureDetailActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PictureAdapterRecyclerView extends RecyclerView.Adapter<PictureAdapterRecyclerView.PictureViewHolder>{

    private ArrayList<Picture> pictures;
    private int resource;    // nuestro layout -> cardview
    private Activity activity;

    public PictureAdapterRecyclerView(ArrayList<Picture> pictures, int resource, Activity activity) {
        this.pictures = pictures;
        this.resource = resource;
        this.activity = activity;
    }

    @NonNull
    @Override
    public PictureViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(this.resource, parent, false);
        return new PictureViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final PictureViewHolder holder, final int position) {
        final Picture picture = this.pictures.get(position);
        holder.usernameCard.setText(picture.getUsername());
        holder.timeCard.setText(picture.getTime());
        holder.likeNumberCard.setText(picture.getLikeNumber() + " " + holder.likeTextCard);
        holder.likeCheckCard.setChecked(picture.isLiked());

        Picasso.get().load(picture.getPicture()).into(holder.pictureCard);

        holder.likeCheckCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isChecked = holder.likeCheckCard.isChecked();
                boolean check = false;

                if(isChecked) {
                    picture.setLikeNumber(picture.getLikeNumber() - 1);
                } else {
                    check = true;
                    picture.setLikeNumber(picture.getLikeNumber() + 1);
                }

                holder.likeCheckCard.setChecked(check);
                holder.likeNumberCard.setText((picture.getLikeNumber() + " " + holder.likeTextCard));
            }
        });

        holder.pictureCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, PictureDetailActivity.class);

                // Transition for android >= 5.0
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Explode explode = new Explode();
                    explode.setDuration(1000);
                    activity.getWindow().setExitTransition(explode);
                    activity.startActivity(intent, ActivityOptionsCompat.makeSceneTransitionAnimation(activity, view,
                            activity.getString(R.string.transitionname_picture))
                            .toBundle());
                } else {
                    activity.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.pictures.size();
    }

    // Clase inner
    public class PictureViewHolder extends RecyclerView.ViewHolder {

        private ImageView pictureCard;
        private TextView usernameCard;
        private TextView timeCard;
        private TextView likeNumberCard;
        private CheckedTextView likeCheckCard;

        private String likeTextCard;

        public PictureViewHolder(@NonNull View itemView) {
            super(itemView);

            pictureCard     = itemView.findViewById(R.id.pictureCard);
            usernameCard    = itemView.findViewById(R.id.userNameCard);
            timeCard        = itemView.findViewById(R.id.timeCard);
            likeNumberCard  = itemView.findViewById(R.id.likeNumberCard);
            likeCheckCard   = itemView.findViewById(R.id.likeCheckCard);

            likeTextCard  = itemView.getResources().getString(R.string.likenumbercard_card);
        }
    }
}
