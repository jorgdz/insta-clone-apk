package com.jorgdz.instaclone.pojo;

public class Picture {
    private String picture;
    private String username;
    private String time;
    private int likeNumber = 0;
    private boolean liked;

    public Picture(String picture, String username, String time, int likeNumber, boolean liked) {
        this.picture = picture;
        this.username = username;
        this.time = time;
        this.likeNumber = likeNumber;
        this.liked = liked;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getLikeNumber() {
        return likeNumber;
    }

    public void setLikeNumber(int likeNumber) {
        this.likeNumber = likeNumber;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }
}
