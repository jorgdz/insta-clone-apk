package com.jorgdz.instaclone.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckedTextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.jorgdz.instaclone.R;
import com.jorgdz.instaclone.view.fragment.HomeFragment;
import com.jorgdz.instaclone.view.fragment.ProfileFragment;
import com.jorgdz.instaclone.view.fragment.SearchFragment;

public class ContainerActivity extends AppCompatActivity {

    BottomNavigationView bottomNavigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);

        bottomNavigation = findViewById(R.id.bottom_navigation);

        bottomNavigation.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        openFragment(HomeFragment.newInstance("", ""));
    }

    BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    openFragment(HomeFragment.newInstance("", ""));
                    return true;
                case R.id.navigation_profile:
                    openFragment(ProfileFragment.newInstance("", ""));
                    return true;
                case R.id.navigation_search:
                    openFragment(SearchFragment.newInstance("", ""));
                    return true;
            }

            return false;
        }
    };

    public void openFragment (Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}